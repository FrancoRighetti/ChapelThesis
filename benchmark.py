import json
import math
import os
import subprocess
from collections import namedtuple
from enum import Enum, auto
from pathlib import Path

import click
import psutil
from rich.console import Console
from rich.pretty import pprint
from sh import Command, docker, gcc, mpicc, mpirun

CORE_COUNT = psutil.cpu_count(logical=False)
THREAD_COUNT = psutil.cpu_count()


class Project(Enum):
    MANDELBROT = auto()
    MATMUL = auto()
    NBODY = auto()
    ODD_EVEN = auto()


class Scaling(Enum):
    STRONG = auto()
    WEAK = auto()


class Technology(Enum):
    CHAPEL = auto()
    MPI = auto()
    OMP = auto()


BINARIES = {
    Project.MANDELBROT: {
        Technology.CHAPEL: "./mandelbrot/chapel/mandelbrot.chpl.out",
        Technology.OMP: "./mandelbrot/omp/mandelbrot.c.out",
    },
    Project.MATMUL: {
        Technology.CHAPEL: "./matmul/chapel/matmul.chpl.out",
        Technology.OMP: "./matmul/omp/matmul.c.out",
    },
    Project.NBODY: {
        Technology.CHAPEL: "./nbody/chapel/nbody.chpl.out",
        Technology.MPI: "./nbody/mpi/nbody.c.out",
    },
    Project.ODD_EVEN: {
        Technology.CHAPEL: "./odd-even/chapel/odd-even.chpl.out",
        Technology.MPI: "./odd-even/mpi/odd-even.c.out",
    },
}


CHAPEL_ARGUMENTS = {
    Project.MANDELBROT: "--imageHeight",
    Project.MATMUL: "--matrixSize",
    Project.NBODY: "--bodyCount",
    Project.ODD_EVEN: "--arraySize",
}


PARAMETERS = {
    Project.MANDELBROT: {
        Scaling.STRONG: [(x, 512) for x in range(1, THREAD_COUNT + 1)],
        Scaling.WEAK: [
            (x, int(256 * math.sqrt(x))) for x in range(1, THREAD_COUNT + 1)
        ],
    },
    Project.MATMUL: {
        Scaling.STRONG: [(x, 2000) for x in range(1, THREAD_COUNT + 1)],
        Scaling.WEAK: [(x, int(250 * (x ** (1/3)))) for x in range(1, THREAD_COUNT + 1)],
    },
    Project.NBODY: {
        Scaling.STRONG: [(x, 12000) for x in range(1, CORE_COUNT + 1)],
        Scaling.WEAK: [(x, 3000 * x) for x in range(1, CORE_COUNT + 1)],
    },
    Project.ODD_EVEN: {
        Scaling.STRONG: [(x, 2400000) for x in range(1, CORE_COUNT + 1)],
        Scaling.WEAK: [(x, 600000 * x) for x in range(1, CORE_COUNT + 1)],
    },
}

RESULT_LAYOUTS = {
    Project.MANDELBROT: [
        "worker_count",
        "elapsed_time",
        "image_height",
        "iteration_limit",
    ],
    Project.MATMUL: [
        "worker_count",
        "elapsed_time",
        "matrix_size",
    ],
    Project.NBODY: [
        "worker_count",
        "elapsed_time",
        "body_count",
        "iteration_count",
    ],
    Project.ODD_EVEN: [
        "worker_count",
        "elapsed_time",
        "array_size",
        "is_sorting_correct",
    ],
}

console = Console()


@click.command()
@click.option("--run-count", default=5)
def main(run_count):
    results = []
    for project in Project:
        for technology, binary_path in BINARIES[project].items():
            for scaling in Scaling:
                for worker_count, parameter in PARAMETERS[project][scaling]:
                    for run_number in range(run_count):
                        output = None
                        match technology:
                            case Technology.CHAPEL:
                                command = get_chapel_command(binary_path)
                                argument = CHAPEL_ARGUMENTS[project]
                                locale_count = (
                                    worker_count
                                    if project is Project.NBODY
                                    or project is Project.ODD_EVEN
                                    else 1
                                )
                                task_count = (
                                    worker_count
                                    if project is Project.MANDELBROT
                                    or project is Project.MATMUL
                                    else 1
                                )
                                output = command(
                                    f"--numLocales={locale_count}",
                                    f"--dataParTasksPerLocale={task_count}",
                                    "--benchmark=true",
                                    f"{argument}={parameter}",
                                )
                            case Technology.MPI:
                                env = os.environ.copy()
                                env["BENCHMARK"] = "true"
                                output = mpirun(
                                    "-np",
                                    worker_count,
                                    binary_path,
                                    parameter,
                                    _env=env,
                                )
                            case Technology.OMP:
                                env = os.environ.copy()
                                env["BENCHMARK"] = "true"
                                env["OMP_NUM_THREADS"] = str(worker_count)
                                output = subprocess.run(
                                    [binary_path, str(parameter)],
                                    capture_output=True,
                                    env=env,
                                    text=True,
                                ).stdout
                        result = zip(RESULT_LAYOUTS[project], output.splitlines())
                        result = dict(result)
                        result["project"] = project.name.lower()
                        result["technology"] = technology.name.lower()
                        result["scaling"] = scaling.name.lower()
                        result["run_number"] = run_number
                        console.print(result)
                        results.append(result)
    with open("results.json", "w") as json_file:
        json.dump(results, json_file, indent=2)


def get_chapel_command(binary_path):
    path = os.path.abspath(binary_path)
    directory = os.path.dirname(path)
    binary_name = os.path.basename(binary_path)
    command = Command("docker").bake(
        "run",
        "--volume",
        f"{directory}:/chapel",
        "chapel/chapel-gasnet",
        f"/chapel/{binary_name}",
    )
    return command


if __name__ == "__main__":
    main()
