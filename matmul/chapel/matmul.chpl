// TODO: Keep all 3 versions (naive, dot, and BLAS)

use LinearAlgebra;
use Random;
use Time;

config const benchmark = false;
config const matrixSize = 1000;

var MatrixDomain = {0..matrixSize - 1, 0..matrixSize - 1};
var p, q, r, qT: [MatrixDomain] real;

// Fill matrices with random values.
var randomStream = createRandomStream(real);
for (i, j) in MatrixDomain {
    p(i, j) = randomStream.getNext();
    q(i, j) = randomStream.getNext();
}

var timer = new Time.Timer();
timer.start();

qT = transpose(q);

// r = p.dot(q);

forall (i, j) in MatrixDomain {
    var s: real = 0.0;
    for k in 0..matrixSize - 1 {
        s += p(i, k) * qT(j, k);
    }
    r(i, j) = s;
}

timer.stop();
var elapsedTime = timer.elapsed();
if benchmark == false {
    writeln("Elapsed time: ", elapsedTime);
} else {
    writeln(dataParTasksPerLocale);
    writeln(elapsedTime);
    writeln(matrixSize);
}
