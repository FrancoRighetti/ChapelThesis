/****************************************************************************
 *
 * omp-mandelbrot.c - displays the Mandelbrot set
 *
 * Copyright (C) 2019, 2020 by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * --------------------------------------------------------------------------
 *
 * This program computes and display the Mandelbrot set. This program
 * requires the gfx library from
 * http://www.nd.edu/~dthain/courses/cse20211/fall2011/gfx (the
 * library should be already included in the archive containing this
 * source file)
 *
 * Compile with
 * gcc -std=c99 -Wall -Wpedantic -fopenmp omp-mandelbrot.c gfx.c -o
 *omp-mandelbrot -lX11
 *
 * Run with:
 * OMP_NUM_THREADS=4 ./omp-mandelbrot
 *
 * If you enable the "runtime" scheduling clause, you can select the
 * scheduling type at runtime, e.g.,
 *
 * OMP_NUM_THREADS=4 OMP_SCHEDULE="static,64" ./omp-mandelbrot
 *
 * At the end, click the left mouse button to close the graphical window.
 *
 ****************************************************************************/

#include "mandelbrot.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <omp.h>

int main(int argc, char *argv[]) {
  bool benchmark = false;
  FILE *out = NULL;
  const char *fname = "omp-mandelbrot.ppm";
  const int thread_count = omp_get_max_threads();
  pixel_t *bitmap = NULL;
  int xsize, ysize;

  ysize = (argc > 1) ? atoi(argv[1]) : 1024;
  xsize = ysize * 1.4;

  char *benchmark_env = getenv("BENCHMARK");
  if (benchmark_env != NULL) {
    benchmark = strcmp(benchmark_env, "true") == 0;
  }

  if (benchmark == false) {
    printf("   Image height: %i\n", ysize);
    printf("Iteration limit: %i\n", MAXIT);
    printf("   Thread count: %i\n", thread_count);

    out = fopen(fname, "w");
    if (!out) {
      fprintf(stderr, "Error: cannot create %s\n", fname);
      return EXIT_FAILURE;
    }

    /* Write the header of the output file */
    fprintf(out, "P6\n");
    fprintf(out, "%d %d\n", xsize, ysize);
    fprintf(out, "255\n");
  }

  /* Allocate the complete bitmap */
  bitmap = (pixel_t *)malloc(xsize * ysize * sizeof(*bitmap));
  assert(bitmap != NULL);

  const double tstart = omp_get_wtime();

#pragma omp parallel default(none) shared(bitmap, thread_count, xsize, ysize)
  {
    const int thread_index = omp_get_thread_num();
    const int local_ysize = ysize / thread_count;
    const int ystart = local_ysize * thread_index;
    const int yend = local_ysize * (thread_index + 1);
    draw_lines(ystart, yend, &bitmap[ystart * xsize], xsize, ysize);
  }

  const double elapsed = omp_get_wtime() - tstart;

  if (benchmark == false) {
    printf("   Elapsed time: %f\n", elapsed);

    fwrite(bitmap, sizeof(*bitmap), xsize * ysize, out);
    fclose(out);
  } else {
    const int thread_count = omp_get_max_threads();
    printf("%i\n", thread_count);
    printf("%f\n", elapsed);
    printf("%i\n", ysize);
    printf("%i\n", MAXIT);
  }

  free(bitmap); /* if bitmap == NULL, does nothing */

  return EXIT_SUCCESS;
}
