from itertools import product

import matplotlib.pyplot as pyplot
import numpy
from rich.console import Console
from rich.pretty import pprint
from rich.table import Table


def parse_file(file_name):
    results_file = open(file_name, "r")
    lines = results_file.readlines()
    results_file.close()
    outputs = [line.strip().split(" ") for line in lines]
    data = dict()
    for output in outputs:
        x = int(output[0])
        y = float(output[1])
        if x not in data:
            data[x] = []
        data[x].append(y)

    for index in data.keys():
        data[index] = numpy.average(data[index])

    return data


def main():
    versions = ["mpi", "omp"]
    scalings = ["strong", "weak"]
    platform = ["bare-metal", "docker"]

    table = Table(title="Results")

    table.add_column("Version")
    table.add_column("Scaling")
    table.add_column("Thread count", justify="center")
    table.add_column("Bare-metal (average, seconds)")
    table.add_column("Docker (average, seconds)")
    table.add_column("Delta (seconds)")

    combinations = list(product(versions, scalings))
    for (version, scaling) in combinations:
        bare_metal_data = parse_file(
            f"./results/{version}-{scaling}-scaling-bare-metal.txt"
        )
        docker_data = parse_file(f"./results/{version}-{scaling}-scaling-docker.txt")
        for thread_count, entry in bare_metal_data.items():
            bare_metal_time = round(entry, 4)
            docker_time = round(docker_data[thread_count], 4)
            delta = round(docker_time - bare_metal_time, 4)
            with open(
                f"./bare_metal-{version}-{scaling}.dat", "a"
            ) as table_file:
                table_file.write(f"{thread_count} {bare_metal_time}\n")
            with open(
                f"./docker-{version}-{scaling}.dat", "a"
            ) as table_file:
                table_file.write(f"{thread_count} {docker_time}\n")


    console = Console()
    console.print(table)


if __name__ == "__main__":
    main()
