use BlockDist;
use Random;
use Sort;
use Time;

config const arraySize = 120000;
config const benchmark = false;

const excess = ceil(arraySize % numLocales): int;
const extraElements = if excess > 0 then numLocales - excess else 0;
const adjustedArraySize = arraySize + extraElements;

if benchmark == false {
  writeln("  Array size: ", arraySize, " (", adjustedArraySize, ")");
  writeln("Locale count: ", numLocales);
}

const arrayDomain = {0..#adjustedArraySize};
var array: [arrayDomain] real;

const distributedArrayDomain: domain(1) dmapped Block(boundingBox=arrayDomain) = arrayDomain;
var distributedArray: [distributedArrayDomain] real;

const phases = 0..#numLocales;

// These variables will be used to synchronize between locales.
const syncsDomain = {phases, 0..#numLocales};
const distributedSyncsDomain: domain(2) dmapped Block(boundingBox=syncsDomain) = syncsDomain;
var syncs$: [distributedSyncsDomain] sync bool;
for sync$ in syncs$ {
  sync$.reset();
}

// Fill array with random values.
const randomStream = createRandomStream(real);
for i in arrayDomain {
    array[i] = randomStream.getNext();
}
distributedArray = array;

var timer = new Time.Timer();
timer.start();

coforall loc in Locales {
  on loc {
    // Prepare local array.
    var localSubdomain = distributedArray.localSubdomain();
    var localArray = distributedArray[localSubdomain].sorted();
    distributedArray[localSubdomain] = localArray;

    // Prepare partner indices.
    var evenPartnerIndex: int;
    var oddPartnerIndex: int;
    if here.id % 2 == 0 {
      evenPartnerIndex = if here.id < numLocales - 1 then here.id + 1 else -1;
      oddPartnerIndex = if here.id > 0 then here.id - 1 else -1;
    } else {
      evenPartnerIndex = if here.id > 0 then here.id - 1 else -1;
      oddPartnerIndex = if here.id < numLocales - 1 then here.id + 1 else -1;
    }

    for phase in phases {
      // Choose the partner.
      var partnerIndex = if phase % 2 == 0 then evenPartnerIndex else oddPartnerIndex;

      // Only do something if we have a partner.
      if partnerIndex >= 0 {
        var partnerLocale = Locales[partnerIndex];
        var partnerSubdomain = distributedArray.localSubdomain(partnerLocale);

        var mergedArraySize = localSubdomain.size + partnerSubdomain.size;
        var mergedArrayDomain = {0..#mergedArraySize};
        var mergedArray: [mergedArrayDomain] real;

        syncs$(phase, here.id).writeEF(true);
        syncs$(phase, partnerIndex).readFE();

        var localArray = distributedArray[localSubdomain];
        var partnerArray = distributedArray[partnerSubdomain];

        syncs$(phase, here.id).writeEF(true);
        syncs$(phase, partnerIndex).readFE();

        // I didn't find a way of converting the subdomains into domains that
        // can be joined togheter. Chapel says that two rectangular domains
        // can't be joined, but in this case we are working on 1D 'rectangular'
        // domains (from BlockDist), so there's that...
        var i = 0;
        for j in localSubdomain {
          mergedArray[i] = localArray[j];
          i += 1;
        }
        for j in partnerSubdomain {
          mergedArray[i] = partnerArray[j];
          i += 1;
        }

        mergedArray = mergedArray.sorted();

        if here.id < partnerIndex {
          distributedArray[localSubdomain] = mergedArray[0..#localSubdomain.size];
        } else {
          distributedArray[localSubdomain] = mergedArray[localSubdomain.size..];
        }

        // This is a more verbose (and surprisngly, faster) way of doing what we did above.
        // if here.id < partnerIndex {
        //   var low = mergedArray[0..#localSubdomain.size];
        //   for (value, i) in zip(low, localSubdomain) {
        //     distributedArray[i] = value;
        //   }
        // } else {
        //   var high = mergedArray[localSubdomain.size..];
        //   for (value, i) in zip(high, localSubdomain) {
        //     distributedArray[i] = value;
        //   }
        // }
      }
    }
  }
}

timer.stop();
var elapsedTime = timer.elapsed();

var sortedArray = array.sorted();
var isSortingCorrect = sortedArray.equals(distributedArray);

if benchmark == false {
  writeln("Elapsed time: ", elapsedTime);
  writeln("     Correct: ", isSortingCorrect);
} else {
  writeln(numLocales);
  writeln(elapsedTime);
  writeln(arraySize);
  writeln(isSortingCorrect);
}
