chapelEnvironment = Environment(
    BUILDERS={
        "Program": Builder(
            action="""
                docker run \
                --rm \
                --volume ${TARGET.dir.abspath}:/source \
                --workdir /source \
                chapel/chapel-gasnet \
                chpl --fast --output ${TARGET.file} ${SOURCE.file}
            """,
            src_suffix=".chpl",
            suffix=".chpl.out",
        )
    }
)

mpiEnvironment = Environment(
    BUILDERS={
        "Program": Builder(
            action="""
                clang-format -i ${SOURCE} && \
                mpicc -std=c99 -O2 \
                -Wall -Werror -Wextra -Wpedantic \
                ${SOURCE} -o ${TARGET} -lm
            """,
            src_suffix=".c",
            suffix=".c.out",
        )
    }
)

ompEnvironment = Environment(
    BUILDERS={
        "Program": Builder(
            action="""
                clang-format -i ${SOURCE} && \
                gcc -std=c99 -O2 \
                -Wall -Werror -Wextra -Wpedantic \
                -fopenmp ${SOURCE} -o ${TARGET}
            """,
            src_suffix=".c",
            suffix=".c.out",
        )
    }
)

chapelEnvironment.Program("./mandelbrot/chapel/mandelbrot")
mpiEnvironment.Program("./mandelbrot/mpi/mandelbrot")
ompEnvironment.Program("./mandelbrot/omp/mandelbrot")

chapelEnvironment.Program("./matmul/chapel/matmul")
ompEnvironment.Program("./matmul/omp/matmul")

chapelEnvironment.Program("./nbody/chapel/nbody")
mpiEnvironment.Program("./nbody/mpi/nbody")

chapelEnvironment.Program("./odd-even/chapel/odd-even")
mpiEnvironment.Program("./odd-even/mpi/odd-even")
