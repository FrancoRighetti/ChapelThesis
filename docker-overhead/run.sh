#! /bin/bash

   python3 main.py --version omp --runs 5 --scaling strong --output-file ./results/omp-strong-scaling-bare-metal.txt \
&& python3 main.py --version omp --runs 5 --scaling strong --output-file ./results/omp-strong-scaling-docker.txt --docker \
&& python3 main.py --version omp --runs 5 --scaling weak --output-file ./results/omp-weak-scaling-bare-metal.txt \
&& python3 main.py --version omp --runs 5 --scaling weak --output-file ./results/omp-weak-scaling-docker.txt --docker \

   python3 main.py --version mpi --runs 5 --scaling strong --output-file ./results/mpi-strong-scaling-bare-metal.txt \
&& python3 main.py --version mpi --runs 5 --scaling strong --output-file ./results/mpi-strong-scaling-docker.txt --docker \
&& python3 main.py --version mpi --runs 5 --scaling weak --output-file ./results/mpi-weak-scaling-bare-metal.txt \
&& python3 main.py --version mpi --runs 5 --scaling weak --output-file ./results/mpi-weak-scaling-docker.txt --docker \
