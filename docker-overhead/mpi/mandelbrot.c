/****************************************************************************
 *
 * mpi-mandelbrot.c - Draw the Mandelbrot set with MPI
 *
 * Copyright (C) 2017--2021 by Moreno Marzolla <moreno.marzolla(at)unibo.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ****************************************************************************/

/***
% HPC - Insieme di Mandelbrot
% Moreno Marzolla <moreno.marzolla@unibo.it>
% Ultimo aggiornamento: 2021-10-28

![Benoit Mandelbrot (1924--2010)](Benoit_Mandelbrot.jpg)

Il file [mpi-mandelbrot.c](mpi-mandelbrot.c) contiene lo scheletro di
una implementazione MPI dell'algoritmo che calcola l'insieme di
Mandelbrot. Non si tratta di una versione realmente parallela, in
quanto il processo master è l'unico che esegue computazioni. 

Il programma accetta come parametro opzionale la dimensione verticale
dell'immagine, ossia il numero di righe (default 1024). La risoluzione
orizzontale viene calcolata automaticamente dal programma in modo da
includere l'intero insieme. Il programma produce un file
`mandebrot.ppm` contenente una immagine dell'insieme di Mandelbrot in
formato PPM (_Portable Pixmap_). Se non si dispone di un programma per
visualizzare questo formato, lo si può convertire, ad esempio, in PNG
dando sul server il comando:

        convert mandelbrot.ppm mandelbrot.png

Scopo di questo esercizio è quello di sviluppare una versione
realmente parallela, in cui tutti i processi MPI cooperino al calcolo
dell'immagine. Per fare questo, si partizioni l'immagine a blocchi per
righe, dove il numero di blocchi è uguale al numero di processi MPI,
in modo che ogni processo calcoli una porzione dell'immagine come
schematizzato nella Figura 1.

![Figura 1: Esempio di suddivisione del dominio per il calcolo dell'insieme di Mandelbrot con 4 processi MPI](mpi-mandelbrot.png)

Più in dettaglio, ciascun processo calcola una porzione di immagine di
dimensione $\mathit{xsize} \times (\mathit{ysize}/P)$, dove $P$ è il
numero di processi; per questa fase non serve alcuna
comunicazione. Successivamente, il processo 0 assembla le porzioni di
immagine calcolate dai vari processi mediante la funzione
`MPI_Gather()`. Poiché l'immagine è stata decomposta in blocchi per
righe, ciascuna porzione è rappresentata da un array di $(3 \times
\mathit{xsize} \times \mathit{ysize} / P)$ elementi di tipo `MPI_BYTE`
(ogni pixel è composto da 3 byte).

Si assuma che _ysize_ sia un multiplo di $P$; una volta ottenuto un
programma corretto, lo si modifichi per gestire dimensioni verticali
arbitrarie; per fare ciò è possibile delegare al processo 0 il calcolo
della porzione di immagine corrispondente alle ultime `(ysize % P)`
righe, oppure usare `MPI_Gatherv()` per riassemblare l'immagine a
partire da frammenti di dimensione diversa.

Suggerisco di conservare la versione seriale del programma per usarla
come riferimento. Per verificare in modo empirico la correttezza del
programma parallelo, consiglio di confrontare il risultato con quello
prodotto dalla versione seriale: le due immagini devono risultare
identiche byte per byte. Per confrontare due immagini si può usare il
comando `cmp` dalla shell di Linux:

        cmp file1 file2

stampa un messaggio se e solo se `file1` e `file2` differiscono.

Per compilare:

        mpicc -std=c99 -Wall -Wpedantic mpi-mandelbrot.c -o mpi-mandelbrot

Per eseguire:

        mpirun -n NPROC ./mpi-mandelbrot [ysize]

Esempio:

        mpirun -n 4 ./mpi-mandelbrot 800

scrive il risultato sul file `mandelbrot.ppm`

## File

- [mpi-mandelbrot.c](mpi-mandelbrot.c)

***/

#include "mandelbrot.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <mpi.h>

int main( int argc, char *argv[] ) {
    FILE *out = NULL;
    char* fname;
    pixel_t *bitmap = NULL;
    int xsize, ysize;

    int my_rank, comm_sz;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

    ysize = (argc > 1) ? atoi(argv[1]) : 1024;
    xsize = ysize * 1.4;
    fname = (argc > 2) ? argv[2] : "mandelbrot.ppm";

    if ( 0 == my_rank ) {
        printf("     Image height: %i\n", ysize);
        printf("  Iteration limit: %i\n", MAXIT);
        printf("Communicator size: %i\n", comm_sz);
    }

    /* xsize and ysize are known to all processes */
    if ( 0 == my_rank ) {
        out = fopen(fname, "w");
        if ( !out ) {
            fprintf(stderr, "Error: cannot create %s\n", fname);
            MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        }

        /* Write the header of the output file */
        fprintf(out, "P6\n");
        fprintf(out, "%d %d\n", xsize, ysize);
        fprintf(out, "255\n");

        /* Allocate the complete bitmap */
        bitmap = (pixel_t*)malloc(xsize*ysize*sizeof(*bitmap));
        assert(bitmap != NULL);
    }

    const double tstart = MPI_Wtime();

    const int local_ysize = ysize / comm_sz;
    const int ystart = local_ysize * my_rank;
    const int yend = local_ysize * (my_rank + 1);
    pixel_t *local_bitmap = (pixel_t*)malloc(xsize*local_ysize*sizeof(*local_bitmap));
    assert(local_bitmap != NULL);

    draw_lines( ystart, yend, local_bitmap, xsize, ysize );

    MPI_Gather( local_bitmap,           /* sendbuf      */
                xsize*local_ysize*3,    /* sendcount    */
                MPI_BYTE,               /* datatype     */
                bitmap,                 /* recvbuf      */
                xsize*local_ysize*3,    /* recvcount    */
                MPI_BYTE,               /* datatype     */
                0,                      /* root         */
                MPI_COMM_WORLD
                );

    if ( 0 == my_rank ) {
        /* the master computes the last (ysize % comm_sz) lines of the image */
        if ( ysize % comm_sz ) {
            const int skip = local_ysize * comm_sz; /* how many rows to skip */
            draw_lines( skip, ysize,
                        &bitmap[skip*xsize],
                        xsize, ysize );
        }

        const double elapsed = MPI_Wtime() - tstart;
        printf("     Elapsed time: %f\n", elapsed);

        fwrite(bitmap, sizeof(*bitmap), xsize*ysize, out);
        fclose(out);
    }

    free(local_bitmap);
    free(bitmap); /* if bitmap == NULL, does nothing */

    MPI_Finalize();

    return EXIT_SUCCESS;
}
