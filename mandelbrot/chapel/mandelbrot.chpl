use IO;
use Time;

type float = real(32);

config const benchmark = false;
config const imageHeight = 1024;
config const iterationLimit = 10000;

if benchmark == false {
    writeln("   Image height: ", imageHeight);
    writeln("Iteration limit: ", iterationLimit);
    writeln("Maximum task concurrency: ", dataParTasksPerLocale);
}

const imageWidth = (imageHeight * 1.4) : int;

record Color {
  var red: uint(8);
  var green: uint(8);
  var blue: uint(8);
}

const black = new Color(0, 0, 0);

const colors = [
    new Color(66, 30, 15),
    new Color(25, 7, 26),
    new Color(9, 1, 47),
    new Color(4, 4, 73),
    new Color(0, 7, 100),
    new Color(12, 44, 138),
    new Color(24, 82, 177),
    new Color(57, 125, 209),
    new Color(134, 181, 229),
    new Color(211, 236, 248),
    new Color(241, 233, 191),
    new Color(248, 201, 95),
    new Color(255, 170, 0),
    new Color(204, 128, 0),
    new Color(153, 87, 0),
    new Color(106, 52, 3)
];

const colorCount: int = 16;

// The definition is rows first because of the way chapel lays out arrays in memory.
// This allows us to iterate over the array and have it behave as we expect it to.
var bitmap: [0..<imageHeight, 0..<imageWidth] Color;

proc iterate(u: float, v: float): int {
    var x, y: float;
    for iteration in 0..<iterationLimit {
        if x * x + y * y > 2.0 * 2.0 {
            return iteration;
        }
        (x, y) = (x * x - y * y + u, 2.0 * x * y + v);
    }
    return iterationLimit;
}

proc drawLines(yStart: int, yEnd: int, ref p: []) {
    var x, y, i: int;
    for y in yStart..<yEnd {
        for x in 0..<imageWidth {
            const u = (-2.5 + 3.5 * x / (imageWidth - 1)) : float;
            const v = (1 - 2.0 * y / (imageHeight - 1)) : float;
            const iteration = iterate(u, v);
            if iteration < iterationLimit {
                p[y, i] = colors[iteration % colors.size];
            } else {
                p[y, i] = black;
            }
            i = (i + 1) % imageWidth;
        }
    }
}

var timer = new Time.Timer();

timer.start();

// This is the serial version, left here just for reference.
// drawLines(0, imageHeight - 1, bitmap);

coforall threadIndex in 0..<dataParTasksPerLocale {
    const height = imageHeight / dataParTasksPerLocale;
    const yStart = height * threadIndex;
    const yEnd = height * (threadIndex + 1);
    drawLines(yStart, yEnd, bitmap);
}

timer.stop();

var elapsedTime = timer.elapsed();

if benchmark == false {
    writeln("   Elapsed time: ", elapsedTime);

    var file = open("chpl-mandelbrot.ppm", iomode.cw);
    var fileWriter = file.writer(kind=ionative);

    fileWriter.writeln("P6");
    const imageSize = "%i %i".format(imageWidth, imageHeight);
    fileWriter.writeln(imageSize);
    fileWriter.writeln("255");

    for bit in bitmap {
        fileWriter.write(bit);
    }

    fileWriter.close();
    file.close();
} else {
    writeln(dataParTasksPerLocale);
    writeln(elapsedTime);
    writeln(imageHeight);
    writeln(iterationLimit);
}
