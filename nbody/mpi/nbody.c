#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const double EPSILON = 1.0e-9f;
const float DELTA = 1e-6;
const int MAX_BODIES = 50000;

struct vector {
  float x;
  float y;
  float z;
};

float random_between(float a, float b) {
  return a + (rand() / (float)RAND_MAX) * (b - a);
}

void initialize(struct vector *positions, struct vector *velocities,
                int count) {
  for (int i = 0; i < count; i++) {
    positions[i].x = random_between(-1, 1);
    positions[i].y = random_between(-1, 1);
    positions[i].z = random_between(-1, 1);
    velocities[i].x = random_between(-1, 1);
    velocities[i].y = random_between(-1, 1);
    velocities[i].z = random_between(-1, 1);
  }
}

void compute_force(struct vector *positions, struct vector *velocities,
                   int start_index, int end_index, int body_count,
                   float delta) {
  for (int i = start_index; i < end_index; i++) {
    float force_x = 0.0;
    float force_y = 0.0;
    float force_z = 0.0;
    for (int j = 0; j < body_count; j++) {
      const float distance_x = positions[j].x - positions[i].x;
      const float distance_y = positions[j].y - positions[i].y;
      const float distance_z = positions[j].z - positions[i].z;
      const float distance_squared = distance_x * distance_x +
                                     distance_y * distance_y +
                                     distance_z * distance_z + EPSILON;
      const float inverse_distance = 1.0f / sqrtf(distance_squared);
      const float inverse_distance_cubed =
          inverse_distance * inverse_distance * inverse_distance;
      force_x += distance_x * inverse_distance_cubed;
      force_y += distance_y * inverse_distance_cubed;
      force_z += distance_z * inverse_distance_cubed;
    }
    velocities[i].x += delta * force_x;
    velocities[i].y += delta * force_y;
    velocities[i].z += delta * force_z;
  }
}

void integrate_positions(struct vector *positions, struct vector *velocities,
                         int count, float delta) {
  for (int i = 0; i < count; i++) {
    positions[i].x += velocities[i].x * delta;
    positions[i].y += velocities[i].y * delta;
    positions[i].z += velocities[i].z * delta;
  }
}

float kinetic_energy(struct vector *velocities, int count) {
  float kinetic_energy = 0.0;
  for (int i = 0; i < count; i++) {
    kinetic_energy += (velocities[i].x * velocities[i].x) +
                      (velocities[i].y * velocities[i].y) +
                      (velocities[i].z * velocities[i].z);
  }
  return 0.5 * kinetic_energy;
}

int main(int argc, char *argv[]) {
  bool benchmark = false;
  int body_count = 10000;
  int iteration_count = 10;
  int local_rank, comm_size;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &local_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

  if (argc > 3) {
    fprintf(stderr, "Usage: %s [body_count] [iteration_count]\n", argv[0]);
    return EXIT_FAILURE;
  }

  if (argc > 1) {
    body_count = atoi(argv[1]);
  }

  if (body_count > MAX_BODIES) {
    fprintf(stderr, "Fatal: too many bodies\n");
    return EXIT_FAILURE;
  }

  if (argc > 2) {
    iteration_count = atoi(argv[2]);
  }

  if (local_rank == 0) {
    char *benchmark_env = getenv("BENCHMARK");
    if (benchmark_env != NULL) {
      benchmark = strcmp(benchmark_env, "true") == 0;
    }

    if (benchmark == false) {
      printf("Body count: %d\n", body_count);
      printf("Communicator size: %d\n", comm_size);
      printf("Iteration count: %d\n", iteration_count);
    }
  }

  const size_t vector_size = body_count * sizeof(struct vector);
  struct vector *positions = (struct vector *)malloc(vector_size);
  assert(positions != NULL);
  struct vector *velocities = (struct vector *)malloc(vector_size);
  assert(velocities != NULL);

  if (local_rank == 0) {
    initialize(positions, velocities, body_count);
  }

  int float_count = 3 * body_count;
  MPI_Bcast(positions, float_count, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Bcast(velocities, float_count, MPI_FLOAT, 0, MPI_COMM_WORLD);

  const int start_index = (body_count * local_rank) / comm_size;
  const int end_index = (body_count * (local_rank + 1)) / comm_size;

  const int local_body_count = end_index - start_index;
  const int local_float_count = 3 * local_body_count;

  struct vector *local_positions = &positions[start_index];
  struct vector *local_velocities = &velocities[start_index];

  double start_time = MPI_Wtime();

  for (int iteration = 1; iteration <= iteration_count; iteration++) {
    compute_force(positions, velocities, start_index, end_index, body_count,
                  DELTA);
    integrate_positions(local_positions, local_velocities, local_body_count,
                        DELTA);

    MPI_Allgather(local_positions, local_float_count, MPI_FLOAT, positions,
                  local_float_count, MPI_FLOAT, MPI_COMM_WORLD);
    MPI_Allgather(local_velocities, local_float_count, MPI_FLOAT, velocities,
                  local_float_count, MPI_FLOAT, MPI_COMM_WORLD);

    if (local_rank == 0) {
      if (benchmark == false) {
        printf("Iteration %2d of %2d: kinetic_energy = %f\n", iteration,
               iteration_count, kinetic_energy(velocities, body_count));
      }
    }
  }

  if (local_rank == 0) {
    double end_time = MPI_Wtime();
    double elapsed_time = end_time - start_time;
    if (benchmark == false) {
      printf("Elapsed time: %f\n", elapsed_time);
    } else {
      printf("%i\n", comm_size);
      printf("%f\n", elapsed_time);
      printf("%i\n", body_count);
      printf("%i\n", iteration_count);
    }
  }

  free(positions);
  free(velocities);

  MPI_Finalize();

  return EXIT_SUCCESS;
}
