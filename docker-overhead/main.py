import math
from collections import namedtuple
from pathlib import Path

import click
from rich.console import Console
from rich.pretty import pprint
from sh import Command, docker, gcc, mpicc, mpirun

OmpResult = namedtuple("OmpResult", ["elapsed_time", "thread_count"])
MpiResult = namedtuple("MpiResult", ["communicator_size", "elapsed_time"])

console = Console()
directory = Path(__file__).parent

STARTING_IMAGE_HEIGHT = 512
STARTING_IMAGE_WIDTH = STARTING_IMAGE_HEIGHT * 1.4
STARTING_PIXELS_PER_THREAD = STARTING_IMAGE_HEIGHT * STARTING_IMAGE_WIDTH

OMP_DIRECTORY = directory / "omp"
OMP_SOURCE = OMP_DIRECTORY / "mandelbrot.c"
OMP_BINARY = OMP_DIRECTORY / "mandelbrot.o"
OMP_OUTPUT = OMP_DIRECTORY / "mandelbrot.ppm"

MPI_DIRECTORY = directory / "mpi"
MPI_SOURCE = MPI_DIRECTORY / "mandelbrot.c"
MPI_BINARY = MPI_DIRECTORY / "mandelbrot.o"
MPI_OUTPUT = MPI_DIRECTORY / "mandelbrot.ppm"


@click.command()
@click.option("--build", is_flag=True)
@click.option("--version", required=True)
@click.option("--docker", is_flag=True)
@click.option("--scaling", required=True)
@click.option("--runs", default=1)
@click.option("--output-file", required=True)
def main(build, version, docker, scaling, runs, output_file):
    if build:
        if version == "mpi":
            # Compile MPI version.
            console.log(f"Started compiling {MPI_SOURCE}")
            with console.status(f"Compiling {MPI_SOURCE}..."):
                mpicc("-std=c99", "-Wall", "-Wpedantic", MPI_SOURCE, "-o", MPI_BINARY)
            console.log(f"Finished compiling {MPI_SOURCE}")
        if version == "omp":
            # Compile OMP version.
            console.log(f"Started compiling {OMP_SOURCE}")
            with console.status(f"Compiling {OMP_SOURCE}..."):
                gcc(
                    "-std=c99",
                    "-Wall",
                    "-Wpedantic",
                    OMP_SOURCE,
                    "-o",
                    OMP_BINARY,
                    "-fopenmp",
                )
            console.log(f"Finished compiling {OMP_SOURCE}")
    outputs = []
    for run in range(runs):
        thread_counts = range(1, 5)
        image_heights = calculate_image_heights(scaling, thread_counts)
        for (thread_count, image_height) in zip(thread_counts, image_heights):
            output = None
            if version == "mpi":
                output = run_benchmark(
                    version,
                    docker=docker,
                    image_height=image_height,
                    thread_count=thread_count,
                )
                output = parse_mpi_output(output)
            elif version == "omp":
                output = run_benchmark(
                    version,
                    docker=docker,
                    image_height=image_height,
                    thread_count=thread_count,
                )
                output = parse_omp_output(output)
            outputs.append(output)
    console.log("Results:")
    pprint(outputs)
    results_file = open(output_file, "w")
    for output in outputs:
        if isinstance(output, MpiResult):
            results_file.write(f"{output.communicator_size} {output.elapsed_time}\n")
        elif isinstance(output, OmpResult):
            results_file.write(f"{output.thread_count} {output.elapsed_time}\n")
    results_file.close()


def run_benchmark(
    version, docker=False, image_height=STARTING_IMAGE_HEIGHT, thread_count=1
):
    version_string = version.capitalize()
    output = None
    console.log(
        f"{version_string} version started (docker={docker}, image_height={image_height}, thread_count={thread_count})"
    )
    with console.status(
        f"Running {version_string} version (docker={docker}, image_height={image_height}, thread_count={thread_count})..."
    ):
        if version == "mpi":
            output = run_mpi_version(docker, image_height, thread_count)
        elif version == "omp":
            output = run_omp_version(docker, image_height, thread_count)
    console.log(
        f"{version_string} version stopped (docker={docker}, image_height={image_height}, thread_count={thread_count})"
    )
    return str(output)


def run_mpi_version(docker, image_height, thread_count):
    output = None
    if docker:
        docker_mpi = Command("docker").bake(
            "run",
            "--volume",
            f"{directory}:/docker-overhead",
            "overhead",
        )
        output = docker_mpi(
            "mpirun",
            "--allow-run-as-root",
            "-np",
            thread_count,
            "/docker-overhead/mpi/mandelbrot.o",
            image_height,
            f"/docker-overhead/mpi/mandelbrot.ppm-docker",
        )
    else:
        output = mpirun(
            "-np", thread_count, MPI_BINARY, image_height, f"{MPI_OUTPUT}-bare-metal"
        )
    return output


def run_omp_version(docker, image_height, thread_count):
    output = None
    if docker:
        docker_omp = Command("docker").bake(
            "run",
            "--volume",
            f"{directory}:/docker-overhead",
            "-e",
            f"OMP_NUM_THREADS={thread_count}",
            "overhead",
        )
        output = docker_omp(
            "/docker-overhead/omp/mandelbrot.o",
            image_height,
            f"/docker-overhead/omp/mandelbrot.ppm-docker",
        )
    else:
        omp_mandelbrot = Command(OMP_BINARY).bake(
            image_height, f"{OMP_OUTPUT}-bare-metal"
        )
        output = omp_mandelbrot(_env={"OMP_NUM_THREADS": str(thread_count)})
    return output


def parse_mpi_output(output):
    communicator_size = None
    elapsed_time = None
    lines = output.splitlines()
    for line in lines:
        if "Communicator size" in line:
            communicator_size = int(line.split(": ")[1].strip())
        elif "Elapsed time" in line:
            elapsed_time = float(line.split(": ")[1].strip())
    return MpiResult(communicator_size, elapsed_time)


def parse_omp_output(output):
    elapsed_time = None
    thread_count = None
    lines = output.splitlines()
    for line in lines:
        if "Elapsed time" in line:
            elapsed_time = float(line.split(": ")[1].strip())
        elif "Thread count" in line:
            thread_count = int(line.split(": ")[1].strip())
    return OmpResult(elapsed_time, thread_count)


def calculate_image_heights(scaling, thread_counts):
    image_heights = []
    for thread_count in thread_counts:
        image_height = None
        if scaling == "strong":
            image_height = STARTING_IMAGE_HEIGHT
        elif scaling == "weak":
            target_total_pixels = STARTING_PIXELS_PER_THREAD * thread_count
            image_height = int(math.sqrt(target_total_pixels / 1.4))
            # image_width = image_height * 1.4
            # actual_total_pixels = image_height * image_width
            # resulting_pixels_per_thread = actual_total_pixels / thread_count
            # console.log(f"Resulting pixels per thread: {resulting_pixels_per_thread}")
            # console.log(f"Expected pixels per thread: {STARTING_PIXELS_PER_THREAD}")
        image_heights.append(image_height)
    return image_heights


if __name__ == "__main__":
    main()
