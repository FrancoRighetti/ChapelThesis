use AllLocalesBarriers;
use BlockDist;
use Random;
use Time;

type float = real(32);

record Vector {
  var x: float;
  var y: float;
  var z: float;


  operator +(left: Vector, right: Vector) {
    return new Vector(left.x + right.x, left.y + right.y, left.z + right.z);
  }

  operator -(left: Vector, right: Vector) {
    return new Vector(left.x - right.x, left.y - right.y, left.z - right.z);
  }

  operator *(left: Vector, right: float) {
    return new Vector(left.x * right, left.y * right, left.z * right);
  }
}

config const benchmark = false;
config const bodyCount = 10000;
config const iterationCount = 10;

const DELTA: float = 1.0e-6;
const EPSILON: float = 1.0e-9;

const arrayDomain = {1..bodyCount};
const distributedArrayDomain: domain(1) dmapped Block(boundingBox=arrayDomain) = arrayDomain;

var positions: [distributedArrayDomain] Vector;

// Fill vectors with values between 0.0 and 1.0
var randomPositionStream = createRandomStream(float);
for i in arrayDomain {
    positions[i].x = randomPositionStream.getNext();
    positions[i].y = randomPositionStream.getNext();
    positions[i].z = randomPositionStream.getNext();
}

const kineticEnergiesDomain = {0..#numLocales};
const kineticEnergiesDistributedDomain: domain(1) dmapped Block(boundingBox=kineticEnergiesDomain) = kineticEnergiesDomain;
var kineticEnergies: [kineticEnergiesDistributedDomain] float;

coforall loc in Locales do on loc {
    var timer = new Time.Timer();
    var iterationTimer = new Time.Timer();

    const localSubdomain = positions.localSubdomain();
    var velocities: [localSubdomain] Vector;

    var randomVelocityStream = createRandomStream(float);
    for i in localSubdomain {
        velocities[i].x = randomVelocityStream.getNext();
        velocities[i].y = randomVelocityStream.getNext();
        velocities[i].z = randomVelocityStream.getNext();
    }

    if here.id == 0 {
        timer.start();
    }

    for iteration in 1..iterationCount {
        if here.id == 0 && benchmark == false {
            iterationTimer.start();
        }

        var localPositions: [arrayDomain] Vector = positions;

        // Compute forces.
        for i in localSubdomain {
            var force: Vector;
            for j in arrayDomain {
                const distance: Vector = localPositions[j] - localPositions[i];
                const distanceSquared = distance.x ** 2
                                      + distance.y ** 2
                                      + distance.z ** 2
                                      + EPSILON;
                const inverseDistance = 1.0 / sqrt(distanceSquared);
                const inverseDistanceCubed = inverseDistance ** 3;

                force += distance * inverseDistanceCubed;
            }
            velocities[i] += force * DELTA;
        }

        // Integrate positions.
        localPositions[localSubdomain] += velocities * DELTA;

        allLocalesBarrier.barrier();

        positions[localSubdomain] = localPositions[localSubdomain];

        allLocalesBarrier.barrier();

        // Calculate kinetic energy.
        const velocity = velocities[localSubdomain].x ** 2
                       + velocities[localSubdomain].y ** 2
                       + velocities[localSubdomain].z ** 2;
        const kineticEnergy = (+ reduce velocity) * 0.5;
        kineticEnergies[here.id] = kineticEnergy;

        allLocalesBarrier.barrier();

        if here.id == 0 && benchmark == false {
            iterationTimer.stop();
            const elapsedTime = iterationTimer.elapsed();
            iterationTimer.clear();

            const totalKineticEnergy = (+ reduce kineticEnergies);
            writeln("Iteration ", iteration, " finished:");
            writeln("    Kinetic energy: ", totalKineticEnergy);
            writeln("      Elapsed time: ", elapsedTime);
        }

        allLocalesBarrier.barrier();
    }

    if here.id == 0 && benchmark == true {
        timer.stop();
        const elapsedTime = timer.elapsed();

        writeln(numLocales);
        writeln(elapsedTime);
        writeln(bodyCount);
        writeln(iterationCount);
    }
}
