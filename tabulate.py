import functools
import itertools
import json

import matplotlib.pyplot as pyplot
import numpy
from rich.console import Console

console = Console()


def main():
    results = None
    with open("results.json", "r") as json_file:
        results = json.load(json_file)

    projects = set([result["project"] for result in results])
    scalings = set([result["scaling"] for result in results])

    for project in projects:
        # Get only results with matching project.
        project_results = [result for result in results if result["project"] == project]

        grouped_project_results = itertools.groupby(
            project_results,
            lambda result: (
                result["worker_count"],
                result["technology"],
                result["scaling"],
            ),
        )

        averaged_project_results = []
        for (worker_count, technology, scaling), runs in grouped_project_results:
            runs = list(runs)
            run_count = len(runs)
            elapsed_times = [run["elapsed_time"] for run in runs]
            average_elapsed_time = functools.reduce(
                lambda a, b: float(a) + float(b), elapsed_times
            )
            average_elapsed_time /= run_count
            averaged_project_results.append(
                {
                    "project": project,
                    "worker_count": int(worker_count),
                    "technology": technology,
                    "scaling": scaling,
                    "elapsed_time": average_elapsed_time,
                }
            )

        grouped_averaged_project_results = itertools.groupby(
            averaged_project_results,
            lambda result: (
                result["technology"],
                result["scaling"],
            ),
        )

        for (technology, scaling), runs in grouped_averaged_project_results:
            runs = list(runs)
            reference_elapsed_time = runs[0]["elapsed_time"]
            for run in runs:
                elapsed_time = run["elapsed_time"]
                worker_count = run["worker_count"]

                efficiency = None
                speedup = None
                match scaling:
                    case "strong":
                        efficiency = reference_elapsed_time / (
                            run["worker_count"] * elapsed_time
                        )
                        speedup = reference_elapsed_time / elapsed_time
                    case "weak":
                        efficiency = reference_elapsed_time / elapsed_time

                with open(
                    f"./tables/{project}-{technology}-{scaling}-efficiency.dat", "a"
                ) as table_file:
                    table_file.write(f"{worker_count} {efficiency}\n")

                with open(
                    f"./tables/{project}-{technology}-{scaling}-time.dat", "a"
                ) as table_file:
                    table_file.write(f"{worker_count} {elapsed_time}\n")

                if scaling == "strong":
                    with open(
                        f"./tables/{project}-{technology}-speedup.dat", "a"
                    ) as table_file:
                        table_file.write(f"{worker_count} {speedup}\n")


if __name__ == "__main__":
    main()
